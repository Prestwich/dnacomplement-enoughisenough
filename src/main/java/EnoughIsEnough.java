

import java.util.ArrayList;
import java.util.HashMap;

public class EnoughIsEnough {

	private static HashMap<Integer,Integer> Map;

	public static int[] deleteNth(int[] elements, int maxOccurrences) {

		ArrayList<Integer> arrayList = new ArrayList<>();

		if(elements.length > 0) {
			Map = new HashMap<>();
			for (Integer element : elements) {
				Integer value = Map.get(element);
				if (value != null) {
					value++;
				} else {
					value = 1;
				}
				Map.put(element,value);
				if (value <= maxOccurrences) {
					arrayList.add(element);
				}
			}
		}

		return arrayList.stream().mapToInt(i -> i).toArray();
	}
}
