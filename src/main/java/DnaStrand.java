public class DnaStrand {
	public static String makeComplement(String dna) {
		StringBuilder complement = new StringBuilder();
		for(char molecule  :dna.toCharArray()) {
			switch(molecule){
				case 'A':
					complement.append('T');
					break;
				case 'T':
					complement.append('A');
					break;
				case 'C':
					complement.append('G');
					break;
				case 'G':
					complement.append('C');
					break;
			}
		}
		return complement.toString();
	}
}
